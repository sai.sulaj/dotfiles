export PATH="$HOME/.cargo/bin:$PATH"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

export EDITOR="nvim"
export PATH="/Applications/CMake.app/Contents/bin":"$PATH"

if [[ $TMUX_PANE ]]; then
  if [[ ! -d "$HOME/.tmux_history_files" ]]; then
    mkdir "$HOME/.tmux_history_files";
  fi
  HISTFILE=$HOME/.tmux_history_files/.bash_history_tmux_${TMUX_PANE:1}
fi

export PATH="$HOME/.poetry/bin:$PATH"
export PATH_TO_FX="$HOME/java/javafx-sdk-11.0.2/lib"

export PATH="$PATH:/Users/ssulaj/Library/Android/sdk/platform-tools/";
export PATH="/opt/homebrew/opt/openjdk/bin:$PATH"
export PATH="/Users/ssulaj/.cabal/bin:$PATH"
export PATH="/opt/homebrew/Cellar/llvm/13.0.1_1/bin:$PATH"

export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES;
