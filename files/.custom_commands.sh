#!/bin/bash

# ---------- ALIASES ---------- #

alias vim='nvim'
alias vi='vim'

# Search command history.
alias hs="history | grep"

# Jump to programming directory.
alias 2dev="cd ~/Documents/dev"

# ---------- FUNCTIONS ---------- #

function dev-env() {
  tmux \; \
    new-window \; \
    send-keys 'vi' C-m \;
}

# Update pretty much everything.
function maintenance() {
  printf "\n==> Upgrading Brew\n";
  brew upgrade;
  printf "\n==> Updating Brew\n";
  brew update;
  printf "\n==> Upgrading Brew Cask\n";
  brew upgrade --cask;
  printf "\n==> Cleaning Up Brew\n";
  brew cleanup;
  printf "\n==> Updating Vim Packages\n";
  vi +PlugUpgrade +PlugUpdate +CocUpdateSync +TSUpdateSync +qa!;
  printf "\n==> Upgrading Oh My Zsh\n";
  omz update || true;
  reset;
}

# Copy file contents.
function cpf() {
  pbcopy < "$1";
}

# Copy working directory.
function cwd() { pwd | pbcopy; }

# Places one paragraph of Lorem Ipsum in the clipboard.
function lorem() {
  echo "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac feugiat sed lectus vestibulum mattis. Pellentesque dignissim enim sit amet venenatis. Id aliquet risus feugiat in ante metus dictum at tempor. Purus sit amet volutpat consequat. Quisque egestas diam in arcu cursus euismod quis. Consectetur adipiscing elit pellentesque habitant morbi tristique. Molestie at elementum eu facilisis sed odio. Turpis egestas sed tempus urna et. Nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus. Fermentum leo vel orci porta. Rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Felis eget nunc lobortis mattis aliquam faucibus purus in massa." | pbcopy;
}

# Print all commands.
function chelp() {
  echo "### FILE MANIPULATION ###"
  echo "  cpf         -> Copies file contents. First argument is file path."
  echo "### DIRECTORY OPERATIONS ###"
  echo "  2dev        -> Navigate to ~/Documents/dev/ ."
  echo "  cwd         -> Places path to current directory in clipboard.
  echo "### MISC ###"
  echo "  hs          -> search command history. First argument is query."
  echo "  lorem       -> Places one paragraph of lorem ipsum into the clipboard"
  echo "  maintenance -> Update pretty much everything"
}
