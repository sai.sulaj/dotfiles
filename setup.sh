. ./utils.sh

PKG_NAME=''
PKG_POST_INSTALL=''
DOTFILE_NAME=''

printf "Do you wish to install \e[32m\e[1mbrew cask packages?\e[0m";
read -p "" -n 1 -r </dev/tty;
echo;

if [[ $REPLY =~ ^[Yy]$ ]]; then
  ./install_brew_cask.sh
else
  update_status "Skipping \e[32mbrew cask packages\e[0m...\n";
fi

printf "Do you wish to install \e[32m\e[1mbrew packages?\e[0m";
read -p "" -n 1 -r </dev/tty;
echo;

if [[ $REPLY =~ ^[Yy]$ ]]; then
  ./install_brew.sh
else
  update_status "Skipping \e[32mbrew packages\e[0m...\n";
fi

printf "Do you wish to \e[32m\e[1mlink dotfiles?\e[0m";
read -p "" -n 1 -r </dev/tty;
echo;

if [[ $REPLY =~ ^[Yy]$ ]]; then
  ./link_dotfiles.sh
else
  update_status "Skipping \e[32mlinking dotfiles\e[0m...\n";
fi

printf "Do you wish to \e[32m\e[1minstall power level 10k?\e[0m";
read -p "" -n 1 -r </dev/tty;
echo;

if [[ $REPLY =~ ^[Yy]$ ]]; then
  ./install_powerlevel_10k.sh
else
  update_status "Skipping \e[32minstalling power level 10k\e[0m...\n";
fi

echo;
echo "Install fonts from here: https://github.com/romkatv/powerlevel10k-media/raw/master";
echo;
