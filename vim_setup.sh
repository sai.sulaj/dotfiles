mkdir -p $HOME/.config/nvim;

curr_dir=$(pwd);
ln -sf "$curr_dir/vim_files/.vimrc" "$HOME/.vimrc";
ln -sf "$curr_dir/vim_files/coc-settings.json" "$HOME/.config/nvim/coc-settings.json";
ln -sf "$curr_dir/vim_files/init.vim" "$HOME/.config/nvim/init.vim";
