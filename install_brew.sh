. ./utils.sh

PKG_NAME=''
PKG_POST_INSTALL=''

echo;
update_status "Installing Brew Packages";

cat brew_packages.json | jq -acr '.[]' | while read -r ITEM; do
  echo; echo;

  PKG_NAME=$(echo $ITEM | jq -r '.name');
  PKG_POST_INSTALL=$(echo $ITEM | jq -r '.post');

  printf "Do you wish to install \e[32m\e[1m$PKG_NAME?\e[0m";
  read -p "" -n 1 -r </dev/tty;
  echo;

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    update_status "Installing $PKG_NAME...";
    brew install "$PKG_NAME";

    if [[ -z $PKG_POST_INSTALL ]]; then
      continue;
    fi

    echo;
    echo "Postinstall script found: $PKG_POST_INSTALL.";
    echo;
    read -p "Execute?" -n 1 -r </dev/tty;

    if [[ $REPLY =~ ^[Yy]$ ]]; then
      update_status "Running postinstall...";
      eval $PKG_POST_INSTALL;
    else
      update_status "Skipping postinstall.";
    fi
  else
    update_status "Skipping $PKG_NAME."
  fi
done
