call plug#begin('~/.local/share/nvim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'scrooloose/nerdcommenter'
Plug 'airblade/vim-gitgutter'
Plug 'dense-analysis/ale'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'wellle/targets.vim'
Plug 'SidOfc/mkdx'
Plug 'sainnhe/sonokai'
Plug 'machakann/vim-highlightedyank'
Plug 'vimwiki/vimwiki'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

call plug#end()

set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "rust",
    "haskell",
    "javascript",
    "typescript",
    "c",
    "bash",
    "cmake",
    "cpp",
    "css",
    "dockerfile",
    "html",
    "make",
    "python",
    "scss",
    "tsx",
    "verilog",
  },
  highlight = { enable = true, },
  incremental_selection = { enable = true, },
  indent = { enable = true, },
}
EOF

source ~/.vimrc
