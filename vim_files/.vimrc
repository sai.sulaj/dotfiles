filetype plugin indent on
syntax on
set tabstop=2
set shiftwidth=2
set expandtab
set number relativenumber
set wildmenu
set number
set laststatus=2
set updatetime=300
set hidden
set cmdheight=1
set shortmess+=c
set signcolumn=yes
set cursorline
set wrap!
set foldmethod=indent
set foldnestmax=1

" nerdcommenter - Pad comments with a space.
let g:NERDSpaceDelims = 1

" --- Vim-COC ---

" Use <tab> for trigger completion and navigate to the next complete item.
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Enables normal mode in terminal buffers.
:tnoremap <Esc> <C-\><C-n>

" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()

" !-- Vim-COC ---
"
" --- vim-fzf ---

" Rg command with preview window.
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
" Respect .gitignore
let $FZF_DEFAULT_COMMAND = 'rg --files --hidden -g "!.git"'

" !-- vim-fzf ---

" --- Colors ---

" Important!!
if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1
colorscheme sonokai

" Color overrides.
highlight ALEErrorSign ctermfg=Red ctermbg=none guifg=Red guibg=none
highlight ALEWarningSign ctermfg=DarkYellow ctermbg=none guifg=DarkYellow guibg=none
highlight ALEError ctermfg=White ctermbg=Red guifg=White guibg=Red
highlight ALEWarning ctermfg=Black ctermbg=DarkYellow guifg=Black guibg=DarkYellow
highlight CocErrorSign ctermfg=Red ctermbg=none guifg=Red guibg=none
highlight CocWarningSign ctermfg=DarkYellow ctermbg=none guifg=DarkYellow guibg=none
highlight CocError ctermfg=White ctermbg=Red guifg=White guibg=Red
highlight CocWarning ctermfg=Black ctermbg=DarkYellow guifg=Black guibg=DarkYellow
highlight SpellBad ctermfg=Red guifg=Red

highlight CursorLineNr ctermfg=107 cterm=bold guifg=#9ed072 gui=bold

highlight GitGutterAdd ctermfg=LightGreen cterm=bold guifg=LightGreen gui=bold
highlight GitGutterChange ctermfg=Yellow cterm=bold guifg=Yellow gui=bold
highlight GitGutterDelete ctermfg=Red cterm=bold guifg=Red gui=bold
highlight SignColumn ctermbg=none guibg=none
highlight Search ctermfg=Black ctermbg=Yellow cterm=bold ctermfg=Black guibg=Yellow gui=bold
highlight IncSearch ctermfg=Black ctermbg=LightGreen cterm=bold ctermfg=Black guibg=LightGreen gui=bold

" !-- Colors ---

" vimwiki - Use markdown.
let g:vimwiki_list = [{
      \ 'syntax': 'markdown',
      \ 'ext': '.md'
      \}]

" --- ALE ---

nmap <C-k> <Plug>(ale_fix)

let g:ale_sign_error = "◉"
let g:ale_sign_warning = "◉"
let g:ale_echo_msg_error_str="E"
let g:ale_echo_msg_warning_str="W"
let g:ale_echo_msg_format='[%linter%]/[%code%] -> %s [%severity%]'
let g:javascript_plugin_jsdoc = 1
let g:ale_lint_delay = 500
let g:ale_rust_cargo_use_clippy = 1
let g:ale_linters = {
\  'javascript': ['eslint'],
\  'typescript': ['tsserver', 'eslint'],
\  'c': ['ccls'],
\  'cpp': ['ccls'],
\  'sh': ['shell', 'shellcheck']
\}
let g:ale_fixers = {
\  'c': ['clang-format'],
\  'cpp': ['clang-format'],
\  'python': ['black'],
\  'rust': ['rustfmt'],
\  'javascript': ['prettier'],
\  'typescript': ['prettier'],
\  'typescriptreact': ['prettier'],
\  'haskell': ['brittany'],
\}
let g:ale_linters_ignore = {
\  'typescript': ['tslint'],
\  'c': ['cc'],
\  'cpp': ['cc']
\}

" !-- ALE ---
