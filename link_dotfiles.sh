. ./utils.sh

DOTFILE_NAME=''

echo;
update_status "Linking Dotfiles";

for file in $(find ./files); do
  DOTFILE_NAME=$(basename $file);
  if [[ $DOTFILE_NAME == "files" ]]; then
    continue;
  fi

  echo;
  update_status "Linking $DOTFILE_NAME...\n";
  ln -sf "$(pwd)/files/$DOTFILE_NAME" "$HOME/$DOTFILE_NAME";
  update_status "Linked $DOTFILE_NAME.\n";
done
