STATUS_UPDATE_PREFIX="\e[1m\e[35m=>\e[39m"

function update_status {
  printf "$STATUS_UPDATE_PREFIX $1\e[0m";
}
