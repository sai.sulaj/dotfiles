. ./utils.sh

PKG_NAME=''

echo;
update_status "Installing Brew Cask Packages";

cat brew_cask_packages.json | jq -acr '.[]' | while read -r ITEM; do
  echo; echo;

  PKG_NAME=$(echo $ITEM | jq -r '.name');

  printf "Do you wish to install \e[32m\e[1m$PKG_NAME?\e[0m";
  read -p "" -n 1 -r </dev/tty;
  echo;

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    update_status "Installing $PKG_NAME...";
    brew install --cask "$PKG_NAME";
  else
    update_status "Skipping $PKG_NAME."
  fi
done

echo; echo;
update_status "Installed Brew Cask Packages\n";
